import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import GoogleLogin from 'react-google-login';
import './images/my-icons-collection/font/flaticon.css';
import './fonts/vazir-fonts/fonts.css';
import './css/normalize.css';
import 'bootstrap/dist/css/bootstrap.css';
import './css/style.css';
import LOGO from './images/LOGO.png';
import STAR from './images/star.png';

class BodySection extends React.Component {
    constructor(props) {
		super(props);
    }
    render() {
    	return (
			<Init/>
        );
    }
}

class Init extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isInside : 1
		}
	}
	fetchInside() {
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
		try {
			fetch('http://localhost:8080/inside_check', requestOptions)
			.then(resp => resp.json())
			.then(data => this.setState(prevState => ({ isInside : data })));
		} catch (error) {
			localStorage.clear();
		}
		
	}
	componentDidMount() {
		if(localStorage.getItem('jwt')) {
			if(localStorage.getItem('jwt') != '') {
				this.fetchInside();
			}
		}
	}
	render() {
    	return (
			<>
				{ this.state.isInside == 1 &&
					<Signup/>
				}
				{ this.state.isInside == 2 &&
					<Home/>
				}
			</>
        );
    }
}

let notif = (kind = 1, text = '') => {
	var kindClass = '';
	if(kind == 0) kindClass = ' alert-success';
	else if(kind == 1) kindClass = ' alert-info';
	else if(kind == 3) kindClass = ' alert-warning';
	else if(kind == 2) kindClass = ' alert-danger';
	var unmountNotif = (e) => {
		e.preventDefault();
		ReactDOM.render(
			<></>
			, document.getElementById('notif'));
	}
	ReactDOM.render(
		<div className={"alert alert-dismissible fade show" + kindClass}>
			<button onClick={unmountNotif} type="button" className="close">&times;</button>
			<span className="notif-text">{text}</span>
		</div>
		, document.getElementById('notif'));
}

let spinner = (active) => {
	if(active) {
		ReactDOM.render(
			<Modal closable="false">
				<div className="spinner-grow text-info"></div>
			</Modal>
			, document.getElementById('modal'));
	}
	else {
		ReactDOM.render(
			<>
			</>
			, document.getElementById('modal'));
	}
}

class Receipt extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			factor : {
				restaurantName : '',
				orders : [],
				netPrice : 0
			}
		}
	}
	fetchFactor() {
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
		fetch('http://localhost:8080/receipt/' + this.props.index, requestOptions)
			.then(resp => resp.json())
			.then(data => this.setState(prevState => ({ factor : data })));
	}
	componentDidMount() {
		this.fetchFactor();
	}
	render() {
		var orders = [];
		for (var i = 0; i < this.state.factor.orders.length; i++) {
			orders.push(
				<tr>
					<td>{i + 1}</td>
					<td>{this.state.factor.orders[i].foodName}</td>
					<td>{this.state.factor.orders[i].count}</td>
					<td>{this.state.factor.orders[i].price}</td>
				</tr>
			);
		}
		return (
			<div className="container receipt">
				<h4 className="res-name">{this.state.factor.restaurantName}</h4>
				<table className="col-md-12 receipt-factor">
					<tbody>
						<tr>
							<th className="col-md-1">ردیف</th>
							<th className="col-md-5">نام غذا</th>
							<th className="col-md-3">تعداد</th>
							<th className="col-md-3">قیمت</th>
						</tr>
						{orders}
					</tbody>
				</table>
				<div className="col-md-12 net-cost">جمع کل: {this.state.factor.netPrice} تومان</div>
			</div>
		);
	}
}

Receipt.propTypes = {
	index: PropTypes.number
};

class FoodDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			count : 0,
			restaurant : ''
		}
		this.handleBuy = this.handleBuy.bind(this);
		this.handleMinus = this.handleMinus.bind(this);
		this.handlePlus = this.handlePlus.bind(this);
	}
	handleMinus(e) {
		e.stopPropagation();
		e.preventDefault();
		var params = {
			"foodName": this.props.name
		};
		var queryString = Object.keys(params).map(function(key) {
			return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
			method: 'DELETE',
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result;
		fetch('http://localhost:8080/minus?' + queryString, requestOptions)
			.then(response => response.json())
			.then(data => result = data )
			.then(() => {
				if (result == 0) {
					this.setState( prevState => ({ count : prevState.count - 1 }) )
				}
			} );
	}
	handlePlus(e) {
		e.stopPropagation();
		e.preventDefault();
		var params = {
			"foodName": this.props.name
		};
		var queryString = Object.keys(params).map(function(key) {
			return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
			method: 'PUT',
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result;
		fetch('http://localhost:8080/plus?' + queryString, requestOptions)
			.then(response => response.json())
			.then(data => result = data )
			.then(() => {
				if (result == 0) {
					this.setState( prevState => ({ count : prevState.count + 1 }) )
				}
			} );
	}
	handleBuy(e) {
		e.stopPropagation();
		e.preventDefault();
		var params = {
			"foodName": this.props.name,
			"restaurantId": this.props.restaurantId,
			"isParty": this.props.isParty
		};
		var queryString = Object.keys(params).map(function(key) {
			return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
			method: 'PUT',
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result;
		fetch('http://localhost:8080/order?' + queryString, requestOptions)
			.then(response => response.json())
			.then(data => result = data )
			.then(() => {
				if (result == 0) {
					this.setState( prevState => ({ count : prevState.count + 1 }) );
				}
				else if (result == -1) {
					notif(2, "سفارشی از رستورانی دیگر در سبد خرید وجود دارد.");
				}
				else if(result == -2) {
					notif(2, "غذا در سرور موجود نمی باشد.");
				}
			} );
	}
	fetchCount() {
		var orders;
		var curRestaurant;
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
		fetch('http://localhost:8080/profile', requestOptions)
			.then(resp => resp.json())
			.then(data => curRestaurant = data)
			.then(() => {
				if(curRestaurant.cartRestaurant == this.props.restaurantId) {
				fetch('http://localhost:8080/cart', requestOptions)
				.then(resp => resp.json())
				.then(data => {orders = data})
				.then(() => {
					for(var i = 0; i < orders.length; i++) {
						if (orders[i].foodName == this.props.name) {
							this.setState(() => ({ count : orders[i].count }));
						}
					}
				});
			}
		  });
	}
	componentDidMount() {
		this.fetchCount();
	}
	componentWillUnmount() {
		if(this.props.rerender != undefined)
			this.props.rerender();
	}
	render() {
		return (
			<div className="food-detail">
				<h5 className="detail-res-name">{this.props.restaurant}</h5>
				<div className="row">
					<img className="food-thumbnail col-md-4" src={this.props.image} alt={this.props.name}/>
					<div className="row col-md-8">
						<div className="food-name col-md-12 flexer"><div className="flex-item">{this.props.name}</div>
							<div className="rate flex-item">
							{this.props.rate}<img className="star" src={STAR} alt="star"/>
							</div>
						</div>
						<div className="row food-desc">
							{this.props.description}
						</div>
						<div className="row">
							{ this.props.isParty &&
								<>
								<span className="old-price col-md-6">{this.props.oldPrice}</span>
								<span className="new-price col-md-6">{this.props.newPrice}</span>
								</>
							}
							{ !this.props.isParty &&
								<>
								<span className="new-price col-md-6">{this.props.price}</span>
								</>
							}
						</div>
					</div>
				</div>
				<div className="row bottom">
					{ this.props.isParty &&
						<p className="residue col-md-3">موجودی: {this.props.currency}</p>
					}
					<div className="col-md-5">
						{ this.state.count > 0 &&
							<div className="count-set">
								<a href="#" onClick={this.handlePlus} className="icon flaticon-plus"></a>
								<span className="count">{this.state.count}</span>
								<a href="#" onClick={this.handleMinus} className="icon flaticon-minus"></a>
							</div>
						}
					</div>
					<button onClick={this.handleBuy} className="btn btn-info col-md-3">خرید</button>
				</div>
			</div>
		);
	}
}

FoodDetail.propTypes = {
	index: PropTypes.number,
	name: PropTypes.string,
	description: PropTypes.string,
	rate: PropTypes.number,
	price: PropTypes.number,
	image: PropTypes.string,
	restaurant: PropTypes.string,
	restaurantId: PropTypes.string,
	isParty: PropTypes.bool,
	rerender: PropTypes.func
};

class Modal extends React.Component {
	constructor(props) {
		super(props);
		this.closeMod = this.closeMod.bind(this);
	}

	closeMod (e) {
		if(this.props.closable == 'true') {
			ReactDOM.render(
				<></>
				, document.getElementById('modal'));
		}
	}
	
	render() {
		return (
			<div onClick={this.closeMod} className='modal display-block modal-container'>
				<section className="modal-main">
					{this.props.children}
				</section>
			</div>
		);
	}
}

Modal.propTypes = {
	closable: PropTypes.string
}

class ViewProfileCredit extends React.Component {
	constructor(props) {
		super(props);
		this.gotoOrders = this.gotoOrders.bind(this);
		this.state = {
			value : '0',
			credit : 0,
			variation : '1'
		};
		spinner(true);
		this.handleValueChange = this.handleValueChange.bind(this);
		this.handleCreditIncrease = this.handleCreditIncrease.bind(this);
	}
	gotoOrders(e) {
		e.preventDefault();
		ReactDOM.render(<ViewProfileOrders />, document.getElementById('root'));
	}
	handleValueChange(e) {
		e.preventDefault();
		e.persist();
		this.setState(prevState => ({value: e.target.value}))
	}
	handleCreditIncrease(e) {
		e.preventDefault();
		e.persist();
		if(isNaN(this.state.value) || parseInt(this.state.value) <= 0) {
			notif(2, "مقدار اعتبار وارد شده صحیح نیست.");
			return;
		}
		var params = {
		    "upCredit": this.state.value
		};
		var queryString = Object.keys(params).map(function(key) {
    		return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
	        method: 'PUT',
	        headers: { 
	        	'Authorization' : localStorage.getItem('jwt')
	        },
		};
		var result;
	    fetch('http://localhost:8080/recharge?' + queryString, requestOptions)
	        .then(response => response.json())
			.then(data => result = data )
			.then(() => {
				this.setState(prevState => ({credit : result, variation : '2'}));
				notif(0, "اعتبار با موفقیت اضافه شد!");
		});
	}
	componentDidMount() {
		spinner(false);
	}
	render() {
		return (
			<div className="page-container">
				<Header kind="viewProfile" />
				<div className="main-content">
					<Jumbotron kind="viewProfile" variation={this.state.variation} credit={this.state.credit}/>
					<fieldset className="container">
						<div dir="ltr" className="middle btn-group">
							<button className="btn btn-danger" type="button">افزایش اعتبار</button>
							<a className="btn btn-light" onClick={this.gotoOrders}>سفارش‌ها</a>
						</div>
						<div>
							<form>
								<input dir="ltr" type="text" className="col-md-5" onChange={this.handleValueChange} placeholder="میزان افزایش اعتبار"/>
								<input type="submit" className="col-md-2 btn btn-info" onClick={this.handleCreditIncrease} value="افزایش"/>
							</form>
						</div>
					</fieldset>
				</div>
				<Footer/>
			</div>
		);
	}
}

class ViewProfileOrders extends React.Component {
	constructor(props) {
		super(props);
		this.gotoCredit = this.gotoCredit.bind(this);
		this.state = {
			"orders" : []
		};
		spinner(true);
	}
	gotoCredit(e) {
		e.preventDefault();
		ReactDOM.render(<ViewProfileCredit />, document.getElementById('root'));
	}
	fetchOrders() {
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
		fetch('http://localhost:8080/history', requestOptions)
		  .then(resp => resp.json())
		  .then(data => this.setState(prevState => ({ orders : data })))
		  .then(() => spinner(false));
	}
	componentDidMount() {
		this.fetchOrders();
	}
	render() {
		var orders = []
		if(this.state.orders.length > 0) {
			for (var i = this.state.orders.length - 1; i >= 0; i--) {
				orders.push(
				<Order
					key={i}
					index={this.state.orders.length - 1 - i}
					place={i}
					restaurant={this.state.orders[i].restaurant}
					status={this.state.orders[i].status}
				/>);
			}
		}
		return (
			<div className="page-container">
				<Header kind="viewProfile" />
				<div className="main-content">
					<Jumbotron kind="viewProfile" variation="1"/>
					<fieldset className="container">
						<div dir="ltr" className="middle btn-group">
							<a className="btn btn-light" onClick={this.gotoCredit}>افزایش اعتبار</a>
							<button className="btn btn-danger" type="button">سفارش‌ها</button>
						</div>
						{orders}
					</fieldset>
				</div>
				<Footer/>
			</div>
		);
	}
}

class Order extends React.Component {
	constructor(props) {
		super(props);
		this.openModal = this.openModal.bind(this);
		spinner(true);
	}
	openModal(e) {
		e.preventDefault();
		ReactDOM.render(
			<Modal closable="true">
				<Receipt index={this.props.place}/>
			</Modal>
			, document.getElementById('modal'));
	}
	componentDidMount() {
		spinner(false);
	}
	render() {
		return (
			<div className="row order mx-auto">
				<div className="col-md-1">{this.props.index + 1}</div>
				<div className="col-md-7">{this.props.restaurant}</div>
				{ this.props.status == 1 &&
					<div className="col-md-4"><button onClick={this.openModal} className="btn btn-success" type="button">پیک در مسیر</button></div>
				}
				{ this.props.status == 0 &&
					<div className="col-md-4"><button onClick={this.openModal} className="btn btn-info" type="button">در جست‌وجوی پیک</button></div>
				}
				{ this.props.status == 2 &&
					<div className="col-md-4"><button onClick={this.openModal} className="btn btn-warning" type="button">مشاهده فاکتور</button></div>
				}
			</div>
		);
	}
}

Order.propTypes = {
	place: PropTypes.number,
	index: PropTypes.number,
	restaurant: PropTypes.string,
	status: PropTypes.number
}

class RestaurantMenu extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			restaurant : {
				"id" : this.props.id,
				"name" : "",
				"location" : {},
				"logo" : "",
				"menu" : []
			}
		};
		spinner(true);
	}
	fetchRestaurant() {
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
		fetch('http://localhost:8080/restaurant/' + this.props.id, requestOptions)
		  .then(resp => resp.json())
		  .then(data => this.setState(prevState => ({ restaurant : data })))
		  .then(() => spinner(false));
	}
	componentDidMount() {
		this.fetchRestaurant();
	}
	needToRerender = () => {
		this.cart.fetchCart();
	}
	render() {
		var dishes = [];
		for (var i = 0; i < this.state.restaurant.menu.length; i++) {
			dishes.push(
				<Dish
					key={i}
					name={this.state.restaurant.menu[i].name}
					description={this.state.restaurant.menu[i].description}
					rate={this.state.restaurant.menu[i].popularity}
					price={this.state.restaurant.menu[i].price}
					image={this.state.restaurant.menu[i].image}
					restaurant={this.state.restaurant.name}
					restaurantId={this.state.restaurant.id}
					rerender={this.needToRerender}
				/>
			);
		}
		return (
			<div className="page-container">
				<Header kind="restaurantMenu" />
				<div className="main-content">
					<Jumbotron
						kind="restaurantMenu"
						name={this.state.restaurant.name}
						image={this.state.restaurant.logo}
					/>
					<div className="flex-menu">
						<div className="res-cart">
							<Cart variation="2" ref={(child) => this.cart = child} />
						</div>
						<div className="res-menu">
							<h3>منوی غذا</h3>
							<div className="dishes">
								{dishes}
							</div>
						</div>
					</div>
				</div>
				<Footer/>
			</div>
		);
	}
}

RestaurantMenu.propTypes = {
	id: PropTypes.string
}

class Cart extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			orders : [

			],
			netCost : 0
		};
		this.handleOrder = this.handleOrder.bind(this);
	}
	handleOrder(e) {
		e.stopPropagation();
		const requestOptions = {
			method: 'POST',
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result;
		fetch('http://localhost:8080/finalize', requestOptions)
			.then(response => response.json())
			.then(data => result = data )
			.then(() => {
				if (result == 0) {
					this.setState(prevState => ({ orders : [], netCost : 0 }))
					notif(0, "سفارش با موفقیت ثبت شد!");
				}
				else if (result == -1) {
					notif(2, "در سبد خرید سفارشی وجود ندارد.");
				}
				else if (result == -2) {
					notif(2, "موجودی حساب کافی نیست!");
				}
				else if (result == -3) {
					notif(2, "تعداد غذاهای موجود در مهمانی کافی نیست!");
				}
			});
	}
	fetchCart = () => {
		var cart = [];
		this.setState(prevState => ({ orders : [], netCost : 0 }));
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
		fetch('http://localhost:8080/cart', requestOptions)
			.then(resp => resp.json())
			.then(data => cart = data )
			.then( () => {
				var ordersTemp = [];
				var netPrice = 0;
				for (var i = 0; i < cart.length; i++) {
					netPrice += cart[i].price * cart[i].count;
					ordersTemp.push(
						<OrdersCart
							key= {i}
							name= {cart[i].foodName}
							count= {cart[i].count}
							price= {cart[i].price}
							calculateNet= {this.fetchCart}
						/>
					);
				}
				this.setState(prevState => ({ orders : ordersTemp, netCost : netPrice }));
			});
	}
	componentDidMount() {
		this.fetchCart();
	}
	render() {
		return(
			<div className="cart-card">
				<h5>سبد خرید</h5>
				<table className="orders-cart">
					<tbody>
						{this.state.orders}
					</tbody>
				</table>
				<p><b>جمع کل: {this.state.netCost} تومان</b></p>
				<button onClick={this.handleOrder} className="btn btn-info" type="button">تأیید نهایی</button>
			</div>
		);
	};
}

class Dish extends React.Component {
	constructor(props) {
		super(props);
		this.state = { modalShow : false };
		this.handleBuy = this.handleBuy.bind(this);
		this.openModal = this.openModal.bind(this);
	}
	openModal(e) {
		e.preventDefault();
		ReactDOM.render(
			<Modal closable="true">
				<FoodDetail
					name={this.props.name}
					description={this.props.description}
					rate={this.props.rate}
					price={this.props.price}
					image={this.props.image}
					restaurant={this.props.restaurant}
					restaurantId={this.props.restaurantId}
					isParty={false}
					rerender={this.props.rerender}
				/>
			</Modal>
			, document.getElementById('modal'));
	}
	handleBuy(e) {
		e.stopPropagation();
		e.preventDefault();
		var params = {
			"foodName": this.props.name,
			"restaurantId": this.props.restaurantId,
			"isParty": false
		};
		var queryString = Object.keys(params).map(function(key) {
			return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
			method: 'PUT',
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result = '';
		fetch('http://localhost:8080/order?' + queryString, requestOptions)
			.then(response => response.json())
			.then(data => result = data )
			.then(() => {
				this.props.rerender();
				if (result != 0) {
					notif(2, "سفارشی از رستورانی دیگر در سبد خرید وجود دارد.");
				}
				else {
					notif(0, "غذا به سبد خرید افزوده شد.");
				}
			} );
	}
	render() {
		return (
			<div onClick={this.openModal} className="container dish">
				<img className="food-thumbnail" src={this.props.image} alt={this.props.name}/>
				<div className="food-desc">
					<div className="food-name">{this.props.name}</div>
					<div className="rate">
						{this.props.rate}<img className="star" src={STAR} alt="star"/>
					</div>
				</div>
				<p>{this.props.price} تومان</p>
				<button onClick={this.handleBuy} className="btn btn-warning" type="button">افزودن به سبد خرید</button>
			</div>
		);
	}
}

Dish.propTypes = {
	id: PropTypes.string,
	name: PropTypes.string,
	description: PropTypes.string,
	rate: PropTypes.number,
	price: PropTypes.number,
	image: PropTypes.string,
	restaurant: PropTypes.string,
	restaurantId: PropTypes.string,
	rerender: PropTypes.func
}

class OrdersCart extends React.Component {
	constructor(props) {
		super(props);
		this.handleMinus = this.handleMinus.bind(this);
		this.handlePlus = this.handlePlus.bind(this);
	}
	handleMinus(e) {
		e.stopPropagation();
		e.preventDefault();
		var params = {
			"foodName": this.props.name
		};
		var queryString = Object.keys(params).map(function(key) {
			return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
			method: 'DELETE',
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result;
		fetch('http://localhost:8080/minus?' + queryString, requestOptions)
			.then(response => response.json())
			.then(data => result = data )
			.then(() => {
				this.props.calculateNet();
			} );
	}
	handlePlus(e) {
		e.stopPropagation();
		e.preventDefault();
		var params = {
			"foodName": this.props.name
		};
		var queryString = Object.keys(params).map(function(key) {
			return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
			method: 'PUT',
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result;
		fetch('http://localhost:8080/plus?' + queryString, requestOptions)
			.then(response => response.json())
			.then(data => result = data )
			.then(() => {
				this.props.calculateNet();
			} );
	}
	render() {
		return (
			<>
				<tr>
					<td>{this.props.name}</td>
					<td>
							<div className="count-set">
								<a href="#" onClick={this.handlePlus} className="icon flaticon-plus"></a>
								<span className="count">{this.props.count}</span>
								<a href="#" onClick={this.handleMinus} className="icon flaticon-minus"></a>
							</div>
						<p>{this.props.price} تومان</p>
					</td>
				</tr>
			</>
		);
	}
}

OrdersCart.propTypes = {
	name: PropTypes.string,
	count: PropTypes.number,
	price: PropTypes.number,
	calculateNet: PropTypes.func
}

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			restaurants : [],
			isSearch : false,
			partyFoods : [],
			remainingPartyTime : 0,
			searchRes : '',
			searchFood : '',
			page : 0
		}
		spinner(true);
	}
	fetchInitial() {
		var reses = [];
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
    	fetch('http://localhost:8080/restaurant', requestOptions)
		  .then(resp => resp.json())
		  .then(data => reses = data)
		  .then(() => {
			fetch('http://localhost:8080/food_party', requestOptions)
			.then(resp => resp.json())
			.then(data => this.setState(prevState => ({ partyFoods : data, restaurants : reses })))
			.then(() => spinner(false));
		  });
	}
	fetchPartyFoods() {
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
    	fetch('http://localhost:8080/food_party', requestOptions)
		  .then(resp => resp.json())
		  .then(data => this.setState(prevState => ({ partyFoods : data })));
	}
	fetchPartyClock() {
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
    	fetch('http://localhost:8080/food_party_clock', requestOptions)
		  .then(resp => resp.json())
		  .then(data => this.setState(prevState => ({ remainingPartyTime : data })));
	}
	componentDidMount() {
		this.fetchInitial();
		// this.fetchPartyFoods();
		// this.fetchPartyClock();
		this.timerId = setInterval(
    		() => {this.fetchPartyFoods()}
    		, 15 * 60 * 1000
		);
		this.timerId2 = setInterval(
    		() => {this.fetchPartyClock()}
    		, 1000
		);
	}
    componentWillUnmount() {
    	clearInterval(this.timerId);
    	clearInterval(this.timerId2);
	}
	str_pad_left(string,pad,length) {
		return (new Array(length+1).join(pad)+string).slice(-length);
	}
	handleSearchFood = (e) => {
		this.setState({searchFood: e.target.value});
	}
	handleSearchRes = (e) => {
		this.setState({searchRes: e.target.value});
	}
	handleSearchSubmit = (e) => {
		e.preventDefault();
		let valid = this.validateInputs();
		if(valid)
		{
			var params = {
				"food": this.state.searchFood,
				"restaurant": this.state.searchRes,
				"page": 0
			};
			var queryString = Object.keys(params).map(function(key) {
				return key + '=' + params[key]
			}).join('&');
			const requestOptions = {
				method: 'PUT',
				headers: { 
					'Authorization' : localStorage.getItem('jwt')
				},
			};
			var result;
			spinner(true);
			fetch('http://localhost:8080/search?' + queryString, requestOptions)
				.then(response => response.json())
				.then(data => this.setState(prevState => ({ restaurants : data, page : 0, isSearch : true })))
				.then(() => spinner(false));
		}
	}
	validateInputs = () => {
		if (this.state.searchFood == '' && this.state.searchRes == '') {
			notif(2, "فیلدهای جستجو مقداردهی نشده اند.");
			return false;
		}
		return true;
	}
	handleShowMore = (e) => {
		var resCount = this.state.restaurants.length;
		if(this.state.isSearch == false) {
			const requestOptions = {
				headers: { 
					'Authorization' : localStorage.getItem('jwt')
				}
			};
			fetch('http://localhost:8080/page/' + (this.state.page + 1), requestOptions)
			  .then(resp => resp.json())
			  .then(data => this.setState(prevState => ({
				  restaurants : prevState.restaurants.concat(data),
				  page : prevState.page + 1
				})))
			  .then(() => {
				if (resCount == this.state.restaurants.length)  
					notif(2, "نتایج جست و جو به پایان رسیدند.")
				} );
		}
		else {
			var params = {
				"food": this.state.searchFood,
				"restaurant": this.state.searchRes,
				"page":this.state.page + 1
			};
			var queryString = Object.keys(params).map(function(key) {
				return key + '=' + params[key]
			}).join('&');
			const requestOptions = {
				method: 'PUT',
				headers: { 
					'Authorization' : localStorage.getItem('jwt')
				},
			};
			var result;
			fetch('http://localhost:8080/search?' + queryString, requestOptions)
				.then(response => response.json())
				.then(data => this.setState(prevState => ({
					restaurants : prevState.restaurants.concat(data),
					page : prevState.page + 1
				  })))
				.then(() => {
				if (resCount == this.state.restaurants.length)  
					notif(2, "نتایج جست و جو به پایان رسیدند.")
				} );
		}
	}
	render() {
		var restaurants = [];
		var partyFoods = [];
		var minutes = Math.floor(this.state.remainingPartyTime / 60)
		var seconds = this.state.remainingPartyTime - minutes * 60
		var finalTime = this.str_pad_left(minutes,'0',2) + ':' + this.str_pad_left(seconds,'0',2);
		for (var i = 0; i < this.state.restaurants.length; i++)
		{
			restaurants.push(
				<RestaurantCard
					key={i}
					pic={this.state.restaurants[i].logo}
					id={this.state.restaurants[i].id}
					name={this.state.restaurants[i].name}
				/>
			);
		}
		for (var i = 0; i < this.state.partyFoods.length; i++)
		{
			for (var j = 0; j < this.state.partyFoods[i].menu.length; j++)
			{
				partyFoods.push(
					<FoodPartyCard
						key={'.' + i + '.' + j}
						pic={this.state.partyFoods[i].menu[j].image}
						name={this.state.partyFoods[i].menu[j].name}
						description={this.state.partyFoods[i].menu[j].description}
						rate={this.state.partyFoods[i].menu[j].popularity}
						oldPrice={this.state.partyFoods[i].menu[j].oldPrice}
						newPrice={this.state.partyFoods[i].menu[j].price}
						currency={this.state.partyFoods[i].menu[j].count}
						restaurant={this.state.partyFoods[i].name}
						restaurantId={this.state.partyFoods[i].id}
					/>
				);
			}
		}
		return (
			<div className="page-container">
				<Header kind="home" />
				<div className="main-content">
					<Jumbotron kind="home"/>
					<div className="search-pad col-md-5">
						<form>
							<input onChange={this.handleSearchFood} type="text" name="food" className="col-md-4" placeholder="نام غذا"/>
							<input onChange={this.handleSearchRes} type="text" name="restaurant" className="col-md-4" placeholder="نام رستوران"/>
							<input onClick={this.handleSearchSubmit} type="submit" className="col-md-3 btn btn-warning" value="جست‌وجو"/>
						</form>
					</div>
					<div className="food-party">
						<h3>جشن غذا!</h3>
						<div className="remaining-time col-md-2">زمان باقی‌مانده:&nbsp;
							{ partyFoods &&
								finalTime
							}
						</div>
						<div className="party-foods-container">
							<div className="box">
								{partyFoods}
							</div>
						</div>
					</div>
					<div className="restaurants">
						<h3>رستوران‌ها</h3>
						<div className="reses">
							{restaurants}
						</div>
						<button onClick={this.handleShowMore} className="btn btn-info">نمایش بیشتر</button>
					</div>
				</div>
				<Footer/>
			</div>
		)
	}
}

class RestaurantCard extends React.Component {
	constructor(props) {
		super(props);
		this.handleMenu = this.handleMenu.bind(this);
	}
	handleMenu(e) {
		ReactDOM.render(<RestaurantMenu id={this.props.id} />, document.getElementById('root'));
	}
	render() {
		return (
			<div className="res">
				<img className="row res-thumbnail mx-auto" src={this.props.pic} alt={this.props.name}/>
				<div className="res-name mx-auto">{this.props.name}</div>
				<button onClick={this.handleMenu} className="btn btn-warning">نمایش منو</button>
			</div>
		);
	}
}

RestaurantCard.propTypes = {
	pic: PropTypes.string,
	id: PropTypes.string,
	name: PropTypes.string
}

class FoodPartyCard extends React.Component {
	constructor(props) {
		super(props);
		this.handleBuy = this.handleBuy.bind(this);
		this.openModal = this.openModal.bind(this);
	}
	openModal(e) {
		e.preventDefault();
		ReactDOM.render(
			<Modal closable="true">
				<FoodDetail
					image={this.props.pic}
					name={this.props.name}
					rate={this.props.rate}
					description={this.props.description}
					oldPrice={this.props.oldPrice}
					newPrice={this.props.newPrice}
					currency={this.props.currency}
					restaurant={this.props.restaurant}
					restaurantId={this.props.restaurantId}
					isParty={true}
				/>
			</Modal>
			, document.getElementById('modal'));
	}
	handleBuy(e) {
		e.preventDefault();
		e.stopPropagation();
		var params = {
			"foodName": this.props.name,
			"restaurantId": this.props.restaurantId,
			"isParty": true
		};
		var queryString = Object.keys(params).map(function(key) {
			return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
			method: 'PUT',
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result = '';
		fetch('http://localhost:8080/order?' + queryString, requestOptions)
			.then(response => response.json())
			.then(data => result = data )
			.then(() => {
				if (result == -1) {
					notif(2, "سفارشی از رستورانی دیگر در سبد خرید وجود دارد.");
				}
				else if(result == -2) {
					notif(2, "غذا در سرور موجود نمی باشد.");
				}
				else {
					notif(0, "غذا به سبد خرید افزوده شد.");
				}
			} );
	}
	render() {
		return (
			<div onClick={this.openModal} className="party-food-card">
				<div className="row">
					<img className="food-thumbnail col-md-6" src={this.props.pic} alt={this.props.name}/>
					<div className="food-name col-md-6">{this.props.name}<br/>
						<div className="rate">
						{this.props.rate}<img className="star" src={STAR} alt="star"/>
						</div>
					</div>
				</div>
				<div className="row">
					<span className="old-price col-md-6">{this.props.oldPrice}</span>
					<span className="new-price col-md-6">{this.props.newPrice}</span>
				</div>
				<div className="row">
					<p className="residue col-md-6">موجودی: {this.props.currency}</p>
					<button onClick={this.handleBuy} className="btn btn-info col-md-5">خرید</button>
				</div>
				<div className="row">
					<p className="restaurant-name col-md-12">{this.props.restaurant}</p>
				</div>
			</div>
		);
	}
}

FoodPartyCard.propTypes = {
	pic: PropTypes.string,
	name: PropTypes.string,
	description: PropTypes.string,
	rate: PropTypes.number,
	oldPrice: PropTypes.number,
	newPrice: PropTypes.number,
	currency: PropTypes.number,
	restaurant: PropTypes.string,
	restaurantId: PropTypes.string
}

class Login extends React.Component {
	constructor(props) {
		super(props);
		spinner(true);
		this.state = {
			email : '',
			password : '',
		};
	}
	componentDidMount() {
		spinner(false);
	}
	handleEmailChange = (e) => {
		this.setState({email: e.target.value});
	}
	handlePasswordChange = (e) => {
		this.setState({password: e.target.value});
	}
	handleSubmit = (e) => {
		e.preventDefault();
		let valid = this.validateInputs();
		if (valid) {
			var params = {
				"email": this.state.email,
				"password": this.state.password
			};
			var queryString = Object.keys(params).map(function(key) {
				return key + '=' + params[key]
			}).join('&');
			const requestOptions = {
				method: 'PUT',
				headers: { 
					// 'Authorization' : localStorage.getItem('jwt')
				},
			};
			var result;
			spinner(true);
			fetch('http://localhost:8080/login?' + queryString, requestOptions)
				.then(response => response.text())
				.then(
					data => { try {
						 {
							if(data == "")
							{
								notif(2, "رمز یا ایمیل وارد شده اشتباه است.");
								localStorage.clear();
							}
							else
							{
								localStorage.setItem("jwt", data);
							} }
					} catch (error) {
						notif(2, "فرایند ورود با مشکل مواجه شد.");
						localStorage.clear();
					} }
				)
				.then(() => {
					spinner(false);
					ReactDOM.render(<Init />, document.getElementById('root'));
				});
		}

	}
	validateInputs = () => {
		var valid = true;
		if (!(/[a-zA-Z][a-zA-Z0-9]*@[a-zA-Z][a-zA-Z0-9]*.[a-zA-Z][a-zA-Z0-9]*/.test(this.state.email)))
		{
			notif(2, "الگوی ایمیل نادرست است.");
			valid = false;
		}
		if (this.state.password.length < 4)
		{
			notif(2, "طول رمز عبور باید حداقل 4 کاراکتر باشد.");
			valid = false;
		}
		return valid;
	}
	gotoSignup = (e) => {
		e.preventDefault();
		ReactDOM.render(<Signup />, document.getElementById('root'));
	}
	onGoogleSignIn = (googleUser) => {
		let profile = googleUser.getBasicProfile();
		// console.log('ID: ' + profile.getId());
		// console.log('Name: ' + profile.getName());
		// console.log('Image URL: ' + profile.getImageUrl());
		// console.log('Email: ' + profile.getEmail());
		var params = {
			"token": googleUser.getAuthResponse().id_token
		};
		var queryString = Object.keys(params).map(function(key) {
			return key + '=' + params[key]
		}).join('&');
		const requestOptions = {
			method: 'PUT',
			headers: { 
				// 'Authorization' : localStorage.getItem('jwt')
			},
		};
		var result;
		spinner(true);
		fetch('http://localhost:8080/google?' + queryString, requestOptions)
			.then(response => response.text())
			.then(
				data => { try {
					 {
						if(data == "")
						{
							notif(2, "اطلاعات وارد شده اشتباه است.");
							localStorage.clear();
						}
						else
						{
							localStorage.setItem("jwt", data);
						} }
				} catch (error) {
					notif(2, "فرایند ورود با مشکل مواجه شد.");
					localStorage.clear();
				} }
			)
			.then(() => {
				spinner(false);
				ReactDOM.render(<Init />, document.getElementById('root'));
			});
	  }
	  
	render() {
		return (
			<div className="page-container">
				<Header kind="loginSignup" />
				<div className="main-content">
					<Jumbotron kind="loginSignup" />
					<fieldset className="container">
						<div dir="ltr" className="middle btn-group">
							<button className="btn btn-light" onClick={this.gotoSignup}>ثبت نام</button>
							<button className="btn btn-danger" type="button">ورود</button>
						</div>
						<form>
							<input dir="ltr" onChange={this.handleEmailChange} type="email" id="email" name="email" placeholder="ایمیل" /><br/>
							<input dir="ltr" onChange={this.handlePasswordChange} type="password" id="password" name="password" placeholder="رمز عبور" /><br/>
							<input onClick={this.handleSubmit} type="submit" className="col-md-2 btn btn-info" value="ورود" /><br/> یا با استفاده از <br/>
							{/* <input onClick={this.handleSubmit} type="submit" className="col-md-2 btn btn-warning" value="ورود با گوگل" /> */}
							{/* <div className="g-signin2 btn" data-onsuccess={this.onGoogleSignIn}></div> */}
							<span className="btn" dir="ltr">
								<GoogleLogin
									clientId="851108352744-s94af4dv4akkc08mcahfduc1i4r87370.apps.googleusercontent.com"
									buttonText="ورود با گوگل"
									onSuccess={this.onGoogleSignIn}
									onFailure={this.onGoogleSignIn}
									cookiePolicy={'single_host_origin'}
								/>
							</span>
						</form>
					</fieldset>
				</div>
				<Footer/>
			</div>
		);
	}
}

class Signup extends React.Component {
	constructor(props) {
		super(props);
		spinner(true);
		this.state = {
			firstname : '',
			lastname : '',
			email : '',
			phone : '',
			password : ''
		}
	}
	handleFirstNameChange = (e) => {
		this.setState({firstname: e.target.value});
	}
	handleLastNameChange = (e) => {
		this.setState({lastname: e.target.value});
	}
	handleEmailChange = (e) => {
		this.setState({email: e.target.value});
	}
	handlePhoneChange = (e) => {
		this.setState({phone: e.target.value});
	}
	handlePasswordChange = (e) => {
		this.setState({password: e.target.value});
	}
	handleSubmit = (e) => {
		e.preventDefault();
		let valid = this.validateInputs();
		if (valid) {
			var params = {
				"firstName": this.state.firstname,
				"lastName": this.state.lastname,
				"email": this.state.email,
				"phone": this.state.phone,
				"password": this.state.password
			};
			var queryString = Object.keys(params).map(function(key) {
				return key + '=' + params[key]
			}).join('&');
			const requestOptions = {
				method: 'PUT',
				headers: { 
					// 'Authorization' : localStorage.getItem('jwt')
				},
			};
			var result;
			spinner(true);
			fetch('http://localhost:8080/signup?' + queryString, requestOptions)
				.then(response => response.json())
				.then(data => {
					if(data == -1)
					{
						notif(2, "این ایمیل قبلا استفاده شده است.");
					}
					else if(data == 0)
					{
						notif(0, "ثبت نام با موفقیت انجام شد.");
					}
					else {
						notif(2, "ثبت نام با شکست مواجه شد. لطفا دوباره امتحان کنید.");
					}
				} )
				.then(() => spinner(false));
		}
	}
	validateInputs = () => {
		var valid = true;
		if (this.state.firstname.length == 0)
		{
			notif(2, "نام خالی است.");
			valid = false;
		}
		if (this.state.lastname.length == 0)
		{
			notif(2, "نام خانوادگی خالی است.");
			valid = false;
		}
		if (!(/[a-zA-Z][a-zA-Z0-9]*@[a-zA-Z][a-zA-Z0-9]*.[a-zA-Z][a-zA-Z0-9]*/.test(this.state.email)))
		{
			notif(2, "الگوی ایمیل نادرست است.");
			valid = false;
		}
		if ((/\D/.test(this.state.phone)))
		{
			notif(2, "الگوی شماره تلفن نادرست است.");
			valid = false;
		}
		if (this.state.password.length < 4)
		{
			notif(2, "طول رمز عبور باید حداقل 4 کاراکتر باشد.");
			valid = false;
		}
		return valid;
	}
	componentDidMount() {
		spinner(false);
	}
	gotoLogin = (e) => {
		e.preventDefault();
		ReactDOM.render(<Login />, document.getElementById('root'));
	}
	render() {
		return (
			<div className="page-container">
				<Header kind="loginSignup" />
				<div className="main-content">
					<Jumbotron kind="loginSignup" />
					<fieldset className="container">
						<div dir="ltr" className="middle btn-group">
							<button className="btn btn-danger" type="button">ثبت نام</button>
							<button className="btn btn-light" onClick={this.gotoLogin}>ورود</button>
						</div>
						<form>
							<input onChange={this.handleFirstNameChange} type="text" id="firstname" name="firstname" placeholder="نام"/><br/>
							<input onChange={this.handleLastNameChange} type="text" id="lastname" name="lastname" placeholder="نام خانوادگی"/><br/>
							<input dir="ltr" onChange={this.handleEmailChange} type="email" id="email" name="email" placeholder="ایمیل"/><br/>
							<input dir="ltr" onChange={this.handlePhoneChange} type="text" id="phone" name="phone" placeholder="تلفن"/><br/>
							<input dir="ltr" onChange={this.handlePasswordChange} type="password" id="password" name="password" placeholder="رمز عبور"/><br/>
							<input onClick={this.handleSubmit} type="submit" className="col-md-2 btn btn-info" value="ثبت نام"/>
						</form>
					</fieldset>
				</div>
				<Footer/>
			</div>
		);
	}
}

class Jumbotron extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			profile: {
				"id":0,
				"firstName":"",
				"lastName":"",
				"phoneNumber":"",
				"email":"",
				"credit":0,
				"cart":[],
				"factors":[],
				"cartRestaurant":null,
				"deliveryStates":[],
				"rawCart":[],
				"deliveryState":[]
			}
		};
		spinner(true);
	}
	fetchProfile() {
		const requestOptions = {
			headers: { 
				'Authorization' : localStorage.getItem('jwt')
			}
		};
    	fetch('http://localhost:8080/profile', requestOptions)
		  .then(resp => resp.json())
		  .then(data => this.setState(prevState => ({ profile : data })))
		  .then(() => spinner(false) );
	}
	componentDidMount() {
		if (this.props.kind == "viewProfile")
		{
			this.fetchProfile();
		}
	}
	render() {
		return (
			<div>
				{ this.props.kind == "home" &&
					<div className="jumbotron cover">
						<div className="container-fluid">
							<div className="row">
								<div className="col-md-12">
									<i className="logo-hodler">
										<img className="home-logo" src={LOGO} alt="لقمه"/>
									</i>
									<h4>اولین و بزرگترین وبسایت سفارش آنلاین غذا در دانشگاه تهران</h4>
								</div>
							</div>
						</div>
					</div>
				}
				{ this.props.kind == "loginSignup" &&
					<div className="jumbotron cover">
						<div className="container-fluid">
							<div className="row">
								<div className="col-md-12 center-text-jumbotron">
									<h4>اولین و بزرگترین وبسایت سفارش آنلاین غذا در دانشگاه تهران</h4>
								</div>
							</div>
						</div>
					</div>
				}
				{ this.props.kind == "viewProfile" &&
					<div className="jumbotron">
						<div className="container-fluid">
							<div className="row">
								<div className="col-md-9">
									<i className="float-right flaticon-account"></i>
									<h1 className="display-3">{this.state.profile.firstName} {this.state.profile.lastName}</h1>
								</div>
								<div className="col-md-3 container">
									<div className="row col"><i className="flaticon-phone"></i> {this.state.profile.phoneNumber} </div>
									<div className="row col"><i className="flaticon-mail"></i> {this.state.profile.email}</div>
									{ this.props.variation == "1" &&
										<div className="row col"><i className="flaticon-card"></i> {this.state.profile.credit} تومان </div>
									}
									{ this.props.variation == "2" &&
										<div className="row col"><i className="flaticon-card"></i> {this.props.credit} تومان </div>
									}
								</div>
							</div>
						</div>
					</div>
				}
				{ this.props.kind == "restaurantMenu" &&
					<div className="jumbotron jumb-menu">
						<div className="container-fluid">
							<div className="row mx-auto">
								<div className="rest-logo">
									<img className="img-thumbnail img-rounded mx-auto" src={this.props.image} alt={this.props.name}/>
									<br/>
									<h1 className="mx-auto">{this.props.name}</h1>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}
}

Jumbotron.propTypes = {
	kind: PropTypes.string,
	name: PropTypes.string,
	image: PropTypes.string,
	variation: PropTypes.string,
	credit: PropTypes.number
}

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.gotoHome = this.gotoHome.bind(this);
		this.gotoProfileOrders = this.gotoProfileOrders.bind(this);
		this.openModal = this.openModal.bind(this);
		this.handleLogOut = this.handleLogOut.bind(this);
	}
	gotoHome(e) {
		e.preventDefault();
		ReactDOM.render(<Home />, document.getElementById('root'));
	}
	gotoProfileOrders(e) {
		e.preventDefault();
		ReactDOM.render(<ViewProfileOrders />, document.getElementById('root'));
	}
	openModal(e) {
		e.preventDefault();
		ReactDOM.render(
			<Modal closable="true">
				<Cart variation="1"/>
			</Modal>
			, document.getElementById('modal'));
	}
	handleLogOut(e) {
		e.preventDefault();
		localStorage.removeItem("jwt");
		ReactDOM.render(<Login />, document.getElementById('root'));
	}
	render() {
		return (
			<header className="container-fluid">
				<table>
					<tbody>
						{ this.props.kind == "home" &&
							<tr>
								<td>
								<a href="#" onClick={this.openModal} className="icon flaticon-smart-cart"></a>
								<a href="#" onClick={this.gotoProfileOrders}>حساب کاربری</a>
								<a href="#" onClick={this.handleLogOut}>خروج</a>
								</td>
							</tr>
						}
						{ this.props.kind == "loginSignup" &&
							<tr>
								<td>
									{/* <a href="#" onClick={this.gotoHome}>
										<img className="logo" src={LOGO} alt="لقمه" />
									</a> */}
								</td>
							</tr>
						}
						{ this.props.kind == "viewProfile" &&
							<tr>
								<td>
									<a href="#" onClick={this.gotoHome}>
										<img className="logo" src={LOGO} alt="لقمه" />
									</a>
								</td>
								<td>
									<a href="#" onClick={this.openModal} className="icon flaticon-smart-cart"></a>
									<a href="#" onClick={this.handleLogOut}>خروج</a>
								</td>
							</tr>
						}
						{ this.props.kind == "restaurantMenu" &&
							<tr>
								<td>
									<a href="#" onClick={this.gotoHome}>
										<img className="logo" src={LOGO} alt="لقمه" />
									</a>
								</td>
								<td>
									<a href="#" onClick={this.gotoProfileOrders}>حساب کاربری</a>
									<a href="#" onClick={this.handleLogOut}>خروج</a>
								</td>
							</tr>
						}
					</tbody>
				</table>
			</header>
		);
	}
}

Header.propTypes = {
	kind: PropTypes.string,
}

function Footer(props) {
	return (
		<footer>
			<p>&copy; تمامی حقوق متعلق به لقمه است.</p>
		</footer>
	);
}

ReactDOM.render(<BodySection />, document.getElementById('root'));